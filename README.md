# Dataset for cyclist orientation detection :bike:

### Purpose :dart:
The aim of this repository is to introduce a new data set called "CIMAT-Cyclist". CIMAT-Cyclist allows to complement the existing sets for the detection of cyclists, besides being useful to evaluate the capacity of the state of the art models for the detection of the cyclist and his orientation.
<br/>

To reinforce the variety of images contained in the data set, we have included images of our environment, such as urban cyclists in our state, as well as cyclists at sporting events and images obtained from the Internet.

This data set is part of the results of the Master's thesis in Software Engineering: **Seguridad del usuario vulnerable de la carretera mediante la detección y seguimiento del ciclista utilizando Aprendizaje Profundo** of the [Center for Research in Mathematics CIMAT A. C.](https://www.cimat.mx/)

### About CIMAT-Cyclist 🚀


This provides a benchmark for cyclist's orientation detection, "CIMAT-Cyclist" with bounding box based labels according to eight different classes depending on the orientation. Which contains 11, 103 images, of which 6,605 images were collected in approximately 450 videos and images taken from sports events and the streets of the state of Zacatecas, Mexico, while 4,498 additional images were obtained from the web in pages such as pixabay, pexels, freephotos, among others.

"CIMAT-Cyclist" provide 20,229 instances over 11,103 cyclist's images, where 80% of the images were split for the training set and 20% for the test set.

Cyclists are divided into 8 classes according to orientation: **CyclistN, CyclistNE, cyclistE, cyclistSE, cyclistS, cyclistSW, cyclistW and cyclistNW**.


#### Research paper :page_with_curl:

García-Venegas, M., Mercado-Ravell, D.A., Pinedo-Sánchez, L.A. et al. On the safety of vulnerable road users by cyclist detection and tracking. Machine Vision and Applications 32, 109 (2021). https://doi.org/10.1007/s00138-021-01231-4

> Garcia-Venegas, M., Mercado-Ravell, D. A., & Carballo-Monsivais, C. A. (2020). On the safety of vulnerable road users by cyclist orientation detection using Deep Learning. arXiv preprint arXiv:2004.11909. arXiv [On the safety of vulnerable road users by cyclist orientation detection using Deep Learning](https://arxiv.org/pdf/2004.11909.pdf)

### Author

* Marichelo García Venegas (marichelo.garciav@gmail.com)

#### Contributors
* Luis Ángel Pinedo Sánchez (luis.pinedo22@gmail.com)

######  Thesis advisors

* Diego Alberto Mercado Ravell
* Carlos Abraham Carballo Monsivais

### Acknowledgements

We thank the Mexican National Council of Science and Technology CONACyT for the grants given. Also to Brenda Lizeth García Venegas :zap: for contributing to the annotation of the images, and Sportbike Jerez MTB for allowing us to take pictures at their events.
We too are grateful to the resources available on the Internet and to the people and community who upload their images.

- IMAGE-NET.org
- pixabay.com
- pexels.com
- freephotos.cc

Google research: ciclying
- 123rf.com
- firefoxbikes.com
- pinterest.es
- cyclingweekly.com
- bicycling.com
- viator.es
- brujulabike.com
- esmtb.com
- todomountainbike.net
- aquimediosdecomunicacion.com
- esciclismo.com
- costablancabikerace.net
- bbcgoodfood.com
- cyclingmagazine.ca
- dw.com
- liv-cycling.com
- bigstockphoto.com
- theguardian.com
- unsplash.com
- experitour.com
- marca.com/ciclismo
and more...

All of them have allowed us to strengthen this database of cyclists.

## License Agreement :pushpin:

This dataset is made freely available to academic and non-academic entities for non-commercial purposes such as academic research, teaching, scientific publications, or personal experimentation. Permission is granted to use the data given that you agree:

- That the dataset comes "AS IS", without express or implied warranty. Although every effort has been made to ensure accuracy, we (Marichelo García Venegas, Diego A. Mercado Ravell, Luis A. Pinedo Sánchez, Carlos A. Carballo, Center for Research in Mathematics CIMAT A. C.) do not accept any responsibility for errors or omissions.
- That you include a reference to the CIMAT-Cyclist Dataset in any work that makes use of the dataset. For research papers and other publications, cite our publication: 

García-Venegas, M., Mercado-Ravell, D.A., Pinedo-Sánchez, L.A. et al. On the safety of vulnerable road users by cyclist detection and tracking. Machine Vision and Applications 32, 109 (2021). https://doi.org/10.1007/s00138-021-01231-4

- That you do not distribute this dataset or modified versions. It is permissible to distribute derivative works in as far as they are abstract representations of this dataset (such as models trained on it or additional annotations that do not directly include any of our data) and do not allow to recover the dataset or something similar in character.
- That you may not use the dataset or any derivative work for commercial purposes as, for example, licensing or selling the data, or using the data with a purpose to procure a commercial gain.
- That all rights not expressly granted to you are reserved by Center for Research in Mathematics CIMAT, A. C.



